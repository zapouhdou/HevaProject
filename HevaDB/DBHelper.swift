//
//  DBHelper.swift
//  HevaDB
//
//  Created by Pierre Doury on 18/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import Foundation
import SQLite3


/*
 Class who get data from DB.
 You must call initDB function right after object init
*/
public class DBHelper {
   public var db: OpaquePointer?
   public var fileURL : URL?

    public init() {
        
    }
    
    
    public func initDb(file : URL)
    {
      fileURL = file
    }
    
    public func openDB()
    {
        if sqlite3_open(fileURL!.path, &db) != SQLITE_OK {
            print("error opening database")
        }
    }
    
    public typealias step3Completion = (_ success: Bool, _ room: [StepThree]?) -> Void
    public func queryStep3(completion : @escaping step3Completion){
        
        var statement: OpaquePointer?
        var ret : [StepThree]?
        let request =  "Select groupment.id_grp, groupment.nom,  etablissement.nom, sum(data.value) from groupment, place, data, etablissement where groupment.id_grp = place.id_grp and place.id_ets = data.id_ets  and data.id_ets = etablissement.id_ets GROUP BY etablissement.id_ets ORDER BY Groupment.id_grp;"
        
        if sqlite3_prepare_v2(db, request, -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing select: \(errmsg)")
        }
        
        ret = [StepThree]()
        while sqlite3_step(statement) == SQLITE_ROW {
            if let grpName = sqlite3_column_text(statement, 1)
            {
                if let etsName = sqlite3_column_text(statement, 2)
                {
                    let idGrp = sqlite3_column_int64(statement, 0)
                    let val = sqlite3_column_double(statement, 3)
                    ret?.append(StepThree(id: Int(idGrp), nameGrp: String(cString: grpName), nameEts: String(cString: etsName), value: Double(val)))
                }
            }
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error finalizing prepared statement: \(errmsg)")
        }
        statement = nil
        completion(true, ret)
    }
    
    public typealias step2Completion = (_ success: Bool, _ room: [StepTwo]?) -> Void
    public func queryStep2(completion : @escaping step2Completion){
    
        var statement: OpaquePointer?
        var ret : [StepTwo]?
        let request = "Select groupment.nom, SUM(data.value), groupment.id_grp from groupment, place, data where groupment.id_grp = place.id_grp and place.id_ets = data.id_ets GROUP BY groupment.nom order by SUM(data.value)"
        
        
        if sqlite3_prepare_v2(db, request, -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing select: \(errmsg)")
        }
        
        ret = [StepTwo]()
        while sqlite3_step(statement) == SQLITE_ROW {
            if let name = sqlite3_column_text(statement, 0)
            {
                let val = sqlite3_column_double(statement, 1)
                let idGrp = sqlite3_column_int64(statement, 2)
                ret?.append(StepTwo(name: String(cString: name), val: val, id: Int(idGrp)))
            }
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error finalizing prepared statement: \(errmsg)")
        }
        statement = nil
       completion(true, ret)
    }
    
}
