//
//  Groupment.swift
//  HevaProject
//
//  Created by Pierre Doury on 18/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import Foundation
import UIKit

public class StepTwo
{
    public var _name : String?
    public var _value : Double?
    public var _id : Int?
    
    init(name: String, val : Double, id : Int) {
        _name = name
        _value = val
        _id = id
    }
}
public class StepThree
{
    public var _id : Int
    public var _nameGrp : String?
    public var _nameEts : String?
    public var _value : Double?
    
    init(id : Int, nameGrp : String, nameEts: String, value : Double) {
        _id = id
        _nameGrp = nameGrp
        _nameEts = nameEts
        _value = value
    }
}



