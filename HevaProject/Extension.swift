//
//  Extension.swift
//  HevaProject
//
//  Created by Pierre Doury on 18/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import Foundation
import UIKit

func copyBundledSQLiteDB() -> String {
    let sourcePath = Bundle.main.path(forResource: "database", ofType: "sqlite")
    
    let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first as String?
    
    let destinationPath = (documentDirectoryPath! as NSString).appendingPathComponent("database.sqlite")
    
    // If file does not exist in the Documents directory already,
    // then copy it from the bundle.
    if !FileManager().fileExists(atPath: destinationPath) {
        do {
            try FileManager().copyItem(atPath: sourcePath!, toPath: destinationPath)
            
        } catch _ {
        }
    }
    return destinationPath
}

extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}

extension UIColor {
    
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
        
    }
    
}
