//
//  GroupmentCell.swift
//  HevaProject
//
//  Created by Pierre Doury on 18/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import Foundation
import UIKit
import HevaDB

class MinMax {
    var _min : Double?
    var _max : Double?
    
    init(min: Double, max: Double) {
        _min = min
        _max = max
    }
}

class GroupementCell: UITableViewCell {
    let groupementNameField: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = ""
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.bold)
        label.textAlignment = .left
        return label
    }()
    
    var colorView : UIView?
    var idGrp : Int!
    
    var value : Double?
    
    var post: StepTwo? {
        didSet {
            if let name = post?._name
            {
                groupementNameField.text = name
            }
            if let val = post?._value
            {
                value = val
            }
            if let id = post?._id
            {
                idGrp = id
            }
        }
    }
    
    
    var postMaxMinValue : MinMax? {
        didSet {
            if let max = postMaxMinValue?._max
            {
                if let min = postMaxMinValue?._min
                {
                    let range = max - min
                    let additioner: Double = (range/(255.00))
                    
                    if let currentValue = value
                    {
                        let val = currentValue - min
                        
                        let colorValue = val / additioner
                       colorView?.backgroundColor = UIColor.rgb(CGFloat(colorValue), green: CGFloat(colorValue), blue: CGFloat(colorValue), alpha: 1)
                    }
                }
            }

        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupViews(){
        idGrp = Int()
        value = Double()
        colorView = UIView()
        self.addSubview(groupementNameField)
        self.addSubview(colorView!)
        self.addConstraintsWithFormat("H:|-5-[v0(20)]-5-[v1]-5-|", views: colorView!, groupementNameField)
        self.addConstraintsWithFormat("V:|-5-[v0]-5-|", views:  groupementNameField)
        self.addConstraintsWithFormat("V:|-5-[v0]-5-|", views: colorView!)
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
}
