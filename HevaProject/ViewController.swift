//
//  ViewController.swift
//  HevaProject
//
//  Created by Pierre Doury on 18/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import UIKit
import CoreData
import SQLite3
import HevaDB




extension ViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let feedCell = tableView.dequeueReusableCell(withIdentifier: "groupementCell", for: indexPath) as! GroupementCell

        feedCell.post = dataTableView![indexPath.row]
        feedCell.postMaxMinValue = MinMax(min: minValue!, max: maxValue!)
        
        return feedCell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let counter = dataTableView?.count
        {
            return counter
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let feedCell = tableView.cellForRow(at: indexPath) as! GroupementCell


        var filteredResults : [StepThree] = (dataStepThree?.filter { $0._id == feedCell.idGrp })!
        
 
        let names = filteredResults.map { $0._nameEts}
        
        let values =  filteredResults.map { $0._value }
        
       
        let nextVC = GroupementDetailViewController()
        nextVC.navigationItem.title = feedCell.groupementNameField.text
        nextVC.generateCharts(nameArray: names as! [String], valueArray: values as! [Double])
        
       
       self.navigationController?.pushViewController(nextVC, animated: true)
    }
}


class ViewController: UIViewController {
    
    var tableView: UITableView!
    var dataTableView: [StepTwo]?
    
    var dataStepThree: [StepThree]?
    var legendView: LegendView?
    var maxValue : Double?
    var minValue : Double?
    
    
    
    
    func getData()
    {
        let dbHelper = DBHelper()
        let ret = copyBundledSQLiteDB()
        let url = URL(fileURLWithPath: ret)
        dbHelper.initDb(file: url)
        dbHelper.openDB()
        
        dbHelper.queryStep3(completion: {
            (success, data) in
            self.dataStepThree = data
        })
        
        dbHelper.queryStep2(completion: {
            (sucess, data) in
            
            self.dataTableView = [StepTwo]()
            self.dataTableView = data
            
            let min = data?.first
            let max = data?.last
            
            self.maxValue = max?._value
            self.minValue = min?._value
            self.legendView?.setUpGradiant(nbr: (data?.count)!, max: self.maxValue! , min:  self.minValue!)
            self.tableView.reloadData()
            
        })
    }
    
    override func viewDidLoad() {
        
    super.viewDidLoad()
        self.navigationItem.title = "Groupements"
        self.legendView = LegendView(frame: CGRect.zero)
        
        self.tableView = UITableView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.backgroundColor = UIColor.white
        self.tableView.register(GroupementCell.self, forCellReuseIdentifier: "groupementCell")
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.estimatedRowHeight = 44
        self.tableView.isHidden = false
        self.tableView.backgroundColor = UIColor.white
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.view.addSubview(tableView)
        self.view.addSubview(legendView!)
        self.view.addConstraintsWithFormat("H:|[v0]|", views: tableView)
        self.view.addConstraintsWithFormat("H:|[v0]|", views: legendView!)
        self.view.addConstraintsWithFormat("V:|-20-[v0]-5-[v1(80)]-20-|", views: tableView, legendView!)
        
        self.getData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

