//
//  GroupmentDetailView.swift
//  HevaProject
//
//  Created by Pierre Doury on 19/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import Foundation
import UIKit
import Charts


extension GroupementDetailViewController : ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
    }
    
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        print("chartValueNothingSelected")
    }
}

extension GroupementDetailViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let feedCell = tableView.dequeueReusableCell(withIdentifier: "groupementDetailCell", for: indexPath) as! GroupementDetailCell
      
        feedCell.postName = nameArray?[indexPath.row]
        feedCell.postValue = valueArray?[indexPath.row]
        
        return feedCell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let counter = nameArray?.count
        {
            return counter
        }
        return 0;
    }
}

class GroupementDetailViewController: UIViewController{
    
    var chart : PieChartView!
    var tableView : UITableView!
    var nameArray : [String]?
    var valueArray : [Double]?
    
    var chartView : UIView!


    func generateCharts(nameArray : [String], valueArray : [Double])
  {
    chart = PieChartView(frame: CGRect(x: 0, y: 0, width: (self.view.frame.width), height: screenHeight/1.5))
    
    chart.delegate = self
    
    chart.drawEntryLabelsEnabled = false
    //  generate chart data entries
    let track = nameArray
    let money = valueArray
    self.nameArray = nameArray
    self.valueArray = valueArray
    var entries = [PieChartDataEntry]()
    for (index, value) in money.enumerated() {
        let entry = PieChartDataEntry()
        entry.y = value
        entry.label = track[index]
        entries.append( entry)
    }
  
    // 3. chart setup
    let set = PieChartDataSet( values: entries, label: "")
    set.drawIconsEnabled = false
    set.drawValuesEnabled = false
    
    var colors: [UIColor] = []
    
    for _ in 0..<money.count {
        let red = Double(arc4random_uniform(256))
        let green = Double(arc4random_uniform(256))
        let blue = Double(arc4random_uniform(256))
        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
        colors.append(color)
    }
    set.colors = colors
    let data = PieChartData(dataSet: set)
   
    chart.data = data
    chart.noDataText = "No data available"
    // user interaction
    chart.isUserInteractionEnabled = true
    
    let d = Description()
    d.text = ""
    d.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
    chart.chartDescription = d
    chart.centerText = ""
    chart.holeRadiusPercent = 0.2
    chart.transparentCircleColor = UIColor.clear
    chart.delegate = self as ChartViewDelegate
    self.chartView.addSubview(chart)
   
    }
    
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }

    override func viewDidLoad() {
         super.viewDidLoad()
        self.chartView = UIView()
        
      
        nameArray = [String]()
        valueArray = [Double]()
        self.tableView = UITableView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.backgroundColor = UIColor.white
        self.tableView.register(GroupementDetailCell.self, forCellReuseIdentifier: "groupementDetailCell")
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.estimatedRowHeight = 44
        self.tableView.isHidden = false
        self.tableView.backgroundColor = UIColor.white
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.view.addSubview(tableView)
        self.view.addSubview(chartView)
        self.view.addConstraintsWithFormat("H:|-5-[v0]-5-|", views: chartView)
        self.view.addConstraintsWithFormat("H:|[v0]|", views: tableView)
        self.view.addConstraintsWithFormat("V:|-80-[v0]-5-[v1(\(screenHeight/1.5))]-20-|", views:tableView, chartView)
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
