//
//  LegendView.swift
//  HevaProject
//
//  Created by Pierre Doury on 19/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//

import Foundation
import UIKit


class LegendView : UIView {
    
    var gradient : CAGradientLayer?
    let minField: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = ""
         label.numberOfLines = 2
        label.textAlignment = .left
        return label
    }()
    let maxField: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = ""
        label.numberOfLines = 2
        label.textAlignment = .left
        return label
    }()
    
    let legendView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.white
       view.frame = CGRect(x: 0, y: 0, width: 300, height: 20)
        return view
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBehavior()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
   
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func addBehavior() {
        
        let blankView = UIView()
        let blankView1 = UIView()
        
        let spaceView = UIView()
        
        
        self.addSubview(blankView)
        self.addSubview(blankView1)
        self.addSubview(spaceView)
        
        self.addSubview(minField)
        self.addSubview(maxField)
        self.addSubview(legendView)
        self.addConstraintsWithFormat("H:|-[v0(v2)]-[v1(300)]-[v2(v0)]|", views: blankView, legendView, blankView1)
        self.addConstraintsWithFormat("H:|-5-[v0(v2)]-[v1]-[v2(v0)]-5-|", views: minField, spaceView, maxField)
        self.addConstraintsWithFormat("V:|-10-[v0]-5-[v1]|", views: legendView,minField)
        self.addConstraintsWithFormat("V:|-10-[v0]-5-[v1]|", views: legendView,maxField)
        
    }
    
    func setUpGradiant(nbr : Int, max : Double, min : Double)
    {
        
        self.minField.text = "Minimum \n\(min.rounded())"
        self.maxField.text = "Maximum \n\(max.rounded())"
        var arrayNumber = [NSNumber]()
        var arrayColor = [CGColor]()
        
        gradient = CAGradientLayer()
        gradient?.frame = legendView.bounds
       gradient?.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradient?.endPoint = CGPoint(x: 0.0, y: 0.5)
        legendView.layer.addSublayer(gradient!)
        
        var additioner : Double = 0.0
        
        let step1 = 1000 / Double(nbr)
        
        let incrementor : Double = (step1/1000)
        for i in 0..<nbr
        {
           arrayNumber.append(NSNumber(value: (additioner)))
            additioner = additioner + incrementor
        }
        
        var additioner1 : Double = 255.0
        let incrementor1 : Double = Double(255/nbr)
        for i in 0..<nbr
        {
            let color = UIColor.rgb(CGFloat(additioner1), green: CGFloat(additioner1), blue: CGFloat(additioner1), alpha: 1)
            arrayColor.append(color.cgColor)
            additioner1 = additioner1 - incrementor1
        }
        gradient?.colors = arrayColor
        gradient?.locations = arrayNumber
    }
    
}
