//
//  GroupementDetailCell.swift
//  HevaProject
//
//  Created by Pierre Doury on 20/06/2018.
//  Copyright © 2018 Pierre Doury. All rights reserved.
//
import Foundation
import UIKit
import HevaDB


class GroupementDetailCell: UITableViewCell {
    let etablissementNameField: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = ""
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.bold)
        label.textAlignment = .left
        return label
    }()
    let groupementValueField: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = ""
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.bold)
        label.textAlignment = .right
        return label
    }()
   

    
    var postName: String? {
        didSet {
            if let name = postName
            {
                etablissementNameField.text = name
            }
        }
    }
    
    var postValue: Double? {
        didSet {
            if let value = postValue
            {
                groupementValueField.text = "\(value)"
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
       
        self.addSubview(etablissementNameField)
        self.addSubview(groupementValueField)
        self.addConstraintsWithFormat("H:|-5-[v0]-5-[v1]-5-|", views: etablissementNameField, groupementValueField)
        self.addConstraintsWithFormat("V:|-5-[v0]-5-|", views:  etablissementNameField)
        self.addConstraintsWithFormat("V:|-5-[v0]-5-|", views: groupementValueField)
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
}
